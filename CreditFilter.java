package CreditCalc;

import java.util.List;

/**
 * Created by redbu on 17.11.2016.
 */
public class CreditFilter{
    //11
    List<Credit> list;
    public CreditFilter(List list) {
        this.list=list;
    }
    //фильтр по сумме
    void SumFilter( long a, long b){
       list.stream().filter(x->x.getSum()>=a&&x.getSum()<=b).forEach(System.out::println);
    }
    //фильтр по времени
    void TimeFilter(int a, int b){
        list.stream().filter(x->x.getTime()>=a&&x.getTime()<=b).forEach(System.out::println);
    }
    //фильтр по сумме и времени
    void SumTimeFilter(long a, long b,int c, int d){
        list.stream().filter(x->x.getSum()>=a&&x.getSum()<=b&&x.getTime()>=c&&x.getTime()<=d).forEach(System.out::println);
    }
}
