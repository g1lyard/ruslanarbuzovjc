package CreditCalc;
public class Credit {
    //11
    private String name;
    private long sum = 0;
    private int time = 0;
    private double percent =0;
    public Credit(String name,long sum, int time,double percent) { //конструктор
        this.name=name;
        if (sum < 0) {
            System.out.println("Сумма не может быть отрицательной");
        } else {
            this.sum = sum;
        }
        if (time < 0) {
            System.out.println("Время кредита не может быть отрицательно");
        } else {
            this.time = time;
        }
        if (percent < 0) {
            System.out.println("Процент не может быть отрицательным");
        } else {
            this.percent = percent;
        }
    }

    public String getName() {
        return name;
    }

    public long getSum() {
        return sum;
    }

    public int getTime() {
        return time;
    }

    public double getPercent() {
        return percent;
    }

    public void annCalc() { //расчет по аннуитвной схеме и вывод на экран
        double result = (percent/100 * 1 / 12) / (1 - Math.pow((1 + percent/100 * 1 / 12), -time)) * sum;
        System.out.println(name +" Переплата = "+result+" Всего = "+(result+sum)+" Ежемесячный платеж = "+((result+sum)/time) + " по аннуитной схеме");
    }

    public void difCalc() { //расчет по дифференциальной схеме и вывод на экран
        double obsh = 0;
        long tsum = sum;
        long q = sum / time;
        System.out.println(name);
        for (int i = 1; i <= time; i++) {
            obsh = obsh + (tsum * percent/100 * 30 / 365);
            System.out.println("Платеж за месяц "+i+" :"+(q+tsum * percent/100 * 30 / 365));
            tsum = tsum - q;
        }
        System.out.println("Переплата = "+obsh+" Всего = "+(obsh+sum)+" по дифференцируемой схеме");
    }
    public String toString(){
        return name +" "+sum+" "+time+" "+percent;
    }
}
