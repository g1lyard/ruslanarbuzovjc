package CreditCalc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by redbu on 04.11.2016.
 */
public class Application {
    public static void main(String[] args) {
        //1
        List<Credit> credits = new ArrayList<>();
        Credit credit1 = new Credit("Кредит 1",100000, 10,24); //создаем кредит с данными (Название ,Сумма, время, процент)
        Credit credit2 = new Credit("Кредит 2",300000, 12,24);
        Credit credit3 = new Credit("Кредит 3",400000, 15,24);
        credits.add(credit1);
        credits.add(credit2);
        credits.add(credit3);
        credit1.annCalc(); // выполняем расчет по аннуитной схеме
        credit1.difCalc(); //выполняем расчет по дифференциальной схеме
        CreditFilter creditFilter = new CreditFilter(credits);
        creditFilter.SumFilter(1,300000);
    }
}
